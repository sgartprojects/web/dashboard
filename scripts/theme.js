function setTheme(theme){
    let s = document.querySelector(':root');
    s.style.setProperty('--primary', `var(--${theme}--primary)`);
    s.style.setProperty('--secondary', `var(--${theme}--secondary)`);
    s.style.setProperty('--header', `var(--${theme}--header)`);
    s.style.setProperty('--paragraph', `var(--${theme}--paragraph`);
    localStorage.setItem("theme", theme);
}

function setThemeFromStorage(){
    if (localStorage.getItem("theme") === null){
        // Replace "polar" to change the default theme
        localStorage.setItem("theme", "polar");
    }
    setTheme(localStorage.getItem("theme"));
}