const SUCCESS_CODES = [

    200,
    201,
    202,
    203,
    204,
    205,
    206,
    207,
    208,
    226,
    300,
    301,
    302,
    303,
    304,
    305,
    306,
    307,
    308,
    401,
    403
]

async function checkUptime(url, uptime) {
    let error = false;
    let online = true;
    let response;
    if (uptime === null) {
        online = true;
        /*try {
            response = await fetch(url);
            if (SUCCESS_CODES.indexOf(response.statusCode) === -1) {
                online = false;
            }
        }
        catch {
            online = false;
            error = true;
        }*/
    }
    else {
        response = await fetch(uptime);
        if (response.ok) {
            let json = await response.json();
            online = json.status === "up";
        }
        else {
            error = true;
        }
    }

    if (online)
        window.open(url, "_blank");
    else
        pageOffline();
    if (error) {
        console.error("Error while checking uptime - " + response.status);
    }
}
